from django.core.validators import validate_email, validate_slug
from django import forms


class StudentRegistration(forms.Form):  # or forms.ModelForm
    name = forms.CharField()
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput)
